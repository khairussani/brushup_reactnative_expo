import { FC, forwardRef, LegacyRef } from "react";
import { KeyboardAvoidingView, ScrollView, TouchableWithoutFeedback, Keyboard, Platform } from "react-native";

/* CAUTION: cannot be used to wrap existing scrollview or flatlist */

type ScrollType = {
    ref: LegacyRef<ScrollView>
}
// const KeyboardAvoidingWrapper = forwardRef((props, ref: LegacyRef<ScrollView>) => {
    const KeyboardAvoidingWrapper: FC = props => {
    return (
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
            <ScrollView 
                contentContainerStyle={{ flex: 1 }} 
                bounces={false}
                // ref={ref}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    {props.children}
                </TouchableWithoutFeedback>
            </ScrollView>
        </KeyboardAvoidingView>
    );
}

export default KeyboardAvoidingWrapper;
