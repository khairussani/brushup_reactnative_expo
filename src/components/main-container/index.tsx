import { FC } from "react";
import styled from "styled-components/native";

const StyledView = styled.View`
    background-color: ${(props) => props.theme.BACKGROUND};
    flex: 1;
    align-items: center;
    justify-content: center;
    padding: 25px;
    padding-top: 40px;
`;

const MainContainer: FC = props => {
    return <StyledView {...props}>{props.children}</StyledView>
}

export default MainContainer;
