import { FC } from "react";
import styled from "styled-components/native";

const StyledText = styled.Text`
    font-size: 15px;
    color: ${props => props.theme.TEXT};
    text-align: left
`;

const RegularText: FC = props => {
    return <StyledText {...props}>{props.children}</StyledText>
}

export default RegularText;