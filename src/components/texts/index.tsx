import BigText from "./big-text";
import RegularText from "./regular-text";
import SmallText from "./small-text";

export { BigText, RegularText, SmallText };