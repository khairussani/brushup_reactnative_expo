import { FC } from "react";
import { ThemeProvider } from "styled-components/native";
import { useAppSelector } from "../redux/hooks";

const ThemeContainer: FC = ({ children }) => {
    const theme = useAppSelector(state => state.themeColour.theme);
    return <ThemeProvider theme={theme}>
        {children}
    </ThemeProvider>
}

export default ThemeContainer;
