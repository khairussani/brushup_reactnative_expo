import { FC } from "react";
import styled from "styled-components/native";
import { RegularText } from "../texts";

type ButtonType = {
    onPress: () => void;
}

const ButtonView = styled.TouchableOpacity`
    align-items: center;
    background-color: ${props => props.theme.PRIMARY_BTN_BACKGROUND};
    width: 100%;
    padding: 15px;
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    height: 60px;
`;

const RegularButton: FC<ButtonType> = props => {
    return (
        <ButtonView {...props} onPress={props.onPress} >
            <RegularText {...props}>{props.children}</RegularText>
        </ButtonView>
    );
}

export default RegularButton;
