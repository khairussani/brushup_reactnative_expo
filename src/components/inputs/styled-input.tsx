import { FC } from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import { SmallText } from "../texts";
import { Octicons, Ionicons } from '@expo/vector-icons';

type InputType = {
    label: string,
    icon: string,
    isPassword: boolean;
    hidePassword: boolean;
    setHidePassword: (arg: boolean) => void;
}

const InputField = styled.TextInput`
    background-color: ${props => props.theme.INPUT_BACKGROUND}
    padding: 15px;
    padding-left: 55px;
    padding-right: 10px;
    font-size: 16px;
    height: 60px;
    margin-vertical: 3px;
    margin-bottom: 10px;
    color: ${props => props.theme.TEXT_INPUT};
    border-radius: 10px;
`;

const LeftIcon = styled.View`
    left: 15px;
    top: 38px;
    position: absolute;
    z-index: 1;
`;

const StyledOcticons = styled(Octicons)`
    color: ${props => props.theme.TEXT_INPUT};
    font-size: 30px;
`;

const RightIcon = styled.TouchableOpacity`
    right: 15px;
    top: 38px;
    position: absolute;
    z-index: 1;
`;

const StyledIonicons = styled(Ionicons)`
    color: ${props => props.theme.TEXT_INPUT};
    font-size: 30px;
`;

const StyledInput: FC<InputType> = ({ label, icon, isPassword, hidePassword, setHidePassword, ...props}) => {
    return (
        <View>
            <LeftIcon>
                <StyledOcticons name={icon} />
            </LeftIcon>
            <SmallText>{label}</SmallText>
            <InputField {...props} />
            {isPassword && (
                <RightIcon onPress={() => setHidePassword(!hidePassword)}>
                    <StyledIonicons name={hidePassword ? 'md-eye-off' : 'md-eye'} />
                </RightIcon>
            )}
        </View>
    );
}

export default StyledInput;