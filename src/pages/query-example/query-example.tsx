import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { FC, useCallback, useEffect, useRef, useState } from "react";
import { SafeAreaView, TouchableOpacity, View, FlatList } from "react-native";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { Card, CardContainer, CardText, DeleteBtn, DeleteBtnTxt, Input } from "../../components/common-components.styled";
import MainContainer from "../../components/main-container";
import { BigText, SmallText } from "../../components/texts";
import { Staff } from "../../models/api1.model";
import { RootStackParams } from "../../models/root-stack-params.model";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { useAddTaskMutation, useGetAllQuery, useRemoveTaskMutation, useUpdateTaskMutation } from "../../redux/services/api1.service";

const QueryExample: FC = () => {
    const navigation = useNavigation<NativeStackNavigationProp<RootStackParams>>();
    const { data, error, isLoading, isFetching, isSuccess } = useGetAllQuery();
    const [addTask] = useAddTaskMutation();
    const [removeTask] = useRemoveTaskMutation();
    const [updateTask] = useUpdateTaskMutation();
    const [inp, setInp] = useState<string>('');
   
    const onAdd = useCallback(() => {
        if (inp!=='') {
            addFunc(inp)
        }
    }, [inp]);

    const addFunc = async (inp:string) => {
        let obj = {
            Done: false,
            Staff: inp
        }
        await addTask(obj)
    }

    const removeFunc = async (id:number) => {
        await removeTask(id);
    }

    const updateFunc = async (obj: Staff) => {
        await updateTask(obj);
    }

    return <>
    <SafeAreaView style={{flex: 1}}>
        <MainContainer>
            <SmallText>QueryExample page</SmallText>
            <View>
                <Input onChangeText={txt => setInp(txt)} />
                <TouchableOpacity onPress={onAdd}>
                    <BigText>Add</BigText>
                </TouchableOpacity>
            </View>
            {error && <SmallText>Something went wrong</SmallText>}
            {isLoading && <SmallText>...Loading</SmallText>}
            {isFetching && <SmallText>...Fetching</SmallText>}
            {isSuccess && (
                <FlatList
                    data={data} 
                    renderItem={ ({ item }) => <CardContainer>
                            <Card>
                                <BouncyCheckbox
                                    size={25}
                                    fillColor="red"
                                    unfillColor="#FFFFFF"
                                    text={item.Staff}
                                    iconStyle={{ borderColor: "red" }}
                                    isChecked={item.Done}
                                    onPress={(isChecked: boolean) => {
                                        updateFunc({...item, Done: isChecked})}
                                    }
                                    />
                                <CardText>{item.Done}</CardText>
                            </Card> 
                            <DeleteBtn onPress={() => removeFunc(item.id)}>
                                <DeleteBtnTxt>X</DeleteBtnTxt>
                            </DeleteBtn>
                        </CardContainer>}
                    keyExtractor={(item,idx) => idx.toString()}
                />
            )}
            
        </MainContainer>
    </SafeAreaView>
    </>
}

// QueryExample.navigationOptions = {
//     headerTitle: 'hello',
// }

export default QueryExample;



