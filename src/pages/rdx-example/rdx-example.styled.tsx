import styled from "styled-components/native";

export const DarkModeBtn = styled.TouchableOpacity`
    background-color: transparent;
    height: 50px;
    width: 70px;
    border: 1px solid red;
    align-items: center;
    justify-content: center;
`;