import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { createRef, FC, LegacyRef, Ref, useCallback, useEffect, useRef, useState } from "react";
import { SafeAreaView, TouchableOpacity, View, FlatList, Dimensions, ScrollView } from "react-native";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { Card, CardContainer, DeleteBtn, DeleteBtnTxt, Input } from "../../components/common-components.styled";
import { RootStackParams } from "../../models/root-stack-params.model";
import { addOperation, removeOperation, updateOperation } from "../../redux/slices/crud.slice";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { DarkModeBtn } from "./rdx-example.styled";
import { changeTheme } from "../../redux/slices/theme.slice";
import { darkTheme, lightTheme } from "../../constants/theme";
import MainContainer from "../../components/main-container";
import { BigText, RegularText, SmallText } from "../../components/texts";
import StyledInput from "../../components/inputs/styled-input";
import RegularButton from "../../components/buttons/regular-button";
import KeyboardAvoidingWrapper from "../../components/keyboard-avoiding-wrapper";

const RdxExample: FC = () => {
    const navigation = useNavigation<NativeStackNavigationProp<RootStackParams>>();
    const crudList = useAppSelector(state => state.crud.customList);
    const dispatch = useAppDispatch();
    const [input, setInput] = useState('');
    const theme = useAppSelector(state => state.themeColour.theme);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [hidePassword, setHidePassword] = useState(true);
    // const keyboardRef = useRef() ;

    useEffect(() => {
    })

    const keyboardDidShow = () => {
        
    }

    const onAdd = useCallback(() => {
        if (input!=='') {
            dispatch(addOperation(input));
        }
    }, [input])

    return <>
    <SafeAreaView style={{flex: 1}}>
        {/* <KeyboardAvoidingWrapper  > */}
        <MainContainer>
            <SmallText>RdxExample page</SmallText>
            {/* {JSON.stringify(todos)} */}
            <RegularButton onPress={() => navigation.navigate('QueryExample')}>
                Next
            </RegularButton>
            {
                theme.mode === 'light'?
                <DarkModeBtn onPress={() => dispatch(changeTheme(darkTheme))}>
                    <RegularText>Dark Mode</RegularText>
                </DarkModeBtn> :
                <DarkModeBtn onPress={() => dispatch(changeTheme(lightTheme))}>
                    <RegularText>Light Mode</RegularText>
                </DarkModeBtn>
            }
            <View>
                <Input onChangeText={txt => setInput(txt)} />
                {/*example styled input for email */}
                {/* <StyledInput
                    label='email address'
                    placeholder='test@gmail.com'
                    // placeholderTextColor='red'
                    onChangeText={setEmail}
                    value={email}
                    keyboardType='email-address'
                    icon='mail'
                    style={{ marginBottom: 25 }}
                /> */}

                {/*example styled input for password */}
                {/* <StyledInput
                    label='Password'
                    placeholder='******'
                    // placeholderTextColor='red'
                    onChangeText={setPassword}
                    value={password}
                    icon='lock'
                    style={{ marginBottom: 25 }}
                    secureTextEntry={hidePassword}
                    hidePassword={hidePassword}
                    setHidePassword={setHidePassword}
                    isPassword={true}
                /> */}
                <TouchableOpacity onPress={onAdd}>
                    <BigText>Add</BigText>
                </TouchableOpacity>
            </View>
            <FlatList
                data={crudList} 
                renderItem={ ({ item }) => <CardContainer>
                        <Card>
                            <BouncyCheckbox
                                size={25}
                                fillColor="red"
                                unfillColor="#FFFFFF"
                                text={item.text}
                                iconStyle={{ borderColor: "red" }}
                                isChecked={item.done}
                                onPress={(isChecked: boolean) => {
                                        let obj = {...item};
                                        obj.done = isChecked;
                                        dispatch(updateOperation(obj))
                                    }
                                }
                            />
                        </Card> 
                        <DeleteBtn onPress={() => dispatch(removeOperation(item.id))}>
                            <DeleteBtnTxt>X</DeleteBtnTxt>
                        </DeleteBtn>
                    </CardContainer>}
                keyExtractor={(item,idx) => idx.toString()}
            />
        </MainContainer>
        {/* </KeyboardAvoidingWrapper> */}
    </SafeAreaView>
    </>
}

// RdxExample.navigationOptions = {
//     headerTitle: 'hello',
// }

export default RdxExample;



