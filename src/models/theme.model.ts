// import original module declarations
import 'styled-components/native';

// extend it,by default DefaultTheme is empty
declare module 'styled-components' {
    export interface DefaultTheme {
        mode: string;
        BACKGROUND: string;
        PRIMARY_BTN_BACKGROUND: string;
        SECONDARY_BTN_BACKGROUND: string;
        TEXT: string;
        INPUT_BACKGROUND: string;
        TEXT_INPUT: string;
        STATUS_BAR: string;
    }
}