import { DefaultTheme } from "styled-components/native";

export const darkTheme: DefaultTheme = {
    mode: 'dark',
    BACKGROUND: '#212121',
    PRIMARY_BTN_BACKGROUND: 'purple',
    SECONDARY_BTN_BACKGROUND: 'maroon',
    TEXT: 'white',
    STATUS_BAR: 'light-content',
    INPUT_BACKGROUND: 'gray',
    TEXT_INPUT: 'white',
}

export const lightTheme: DefaultTheme = {
    mode: 'light',
    BACKGROUND: 'papayawhip',
    PRIMARY_BTN_BACKGROUND: 'yellow',
    SECONDARY_BTN_BACKGROUND: 'green',
    TEXT: 'black',
    STATUS_BAR: 'default',
    INPUT_BACKGROUND: 'white',
    TEXT_INPUT: 'black'
}
